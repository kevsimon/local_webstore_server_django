"""
@author Mattis Rosenkranz
"""

from django.contrib.auth.models import UserManager as DjangoUserManager


class UserManager(DjangoUserManager):
    pass
