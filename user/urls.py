from rest_framework import routers

from user.boundary.api.general.views import UserViewSet

router = routers.SimpleRouter()
router.register('users', UserViewSet, 'User')
urlpatterns = router.urls
