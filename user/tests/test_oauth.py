"""
@author Mattis Rosenkranz
"""

from local_webstore import settings
from local_webstore.util.test import JsonApiTestCase
from user.entity.models import User


class OauthUserInfoTest(JsonApiTestCase):
    """authentication test class"""
    ENDPOINT_URL = "/api/v1/users"

    def test_get_oauth_token(self):
        self.assertIsNotNone(self.token)

    def test_api_get_userinfo(self):
        # get user data from endpoint using oauth token
        response = self.client.get(f"{self.ENDPOINT_URL}/me/", HTTP_AUTHORIZATION=self.token)

        # check response data
        self.assertEqual(response.status_code, 200)
        attributes = response.json()["data"]["attributes"]
        self.assert_attributes_match(attributes, {
            "email": "",
            "firstName": "",
            "lastName": ""
        })

    def test_create_update_user_with_oauth_token(self):
        # get user data from endpoint using oauth token
        self.client.get(f"{self.ENDPOINT_URL}/me/", HTTP_AUTHORIZATION=self.token)
        user = User.objects.get(email=settings.OIDC_TEST_EMAIL)

        # inline function for checking user attributes by dict
        def check_user_attributes():
            for attribute in settings.OIDC_TEST_EXPECTED_USER_DATA:
                self.assertEqual(getattr(user, attribute), settings.OIDC_TEST_EXPECTED_USER_DATA[attribute])

        # check attributes of created user
        check_user_attributes()

        # update user
        user.first_name = ""
        user.last_name = ""
        user.is_vendor = False
        user.is_staff = False
        user.is_superuser = False
        user.save()

        # hit endpoint again to update user from oauth provider
        self.client.get(f"{self.ENDPOINT_URL}/me/", HTTP_AUTHORIZATION=self.token)

        # check attributes of updated user
        user.refresh_from_db()
        check_user_attributes()
