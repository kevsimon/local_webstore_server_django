"""
@author Mattis Rosenkranz
"""

from django.db import models
from django.contrib.auth.models import AbstractUser

from user.control.managers import UserManager
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    """user model"""
    email = models.EmailField(unique=True)
    first_name = models.CharField(_('first name'), max_length=150)
    last_name = models.CharField(_('last name'), max_length=150)
    is_vendor = models.BooleanField(default=False)

    objects = UserManager()

    def __str__(self):
        return self.get_full_name()
