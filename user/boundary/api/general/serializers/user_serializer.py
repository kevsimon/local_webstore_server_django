"""
@author Mattis Rosenkranz
"""

from rest_framework_json_api import serializers
from user.entity.models import User


class UserSerializer(serializers.ModelSerializer):
    """serializer of user"""
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name']
