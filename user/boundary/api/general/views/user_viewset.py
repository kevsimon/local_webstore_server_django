"""
@author Mattis Rosenkranz
"""

from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework_json_api.views import AutoPrefetchMixin, PreloadIncludesMixin, RelatedMixin

from user.entity.models import User
from user.boundary.api.general.serializers import UserSerializer


class UserViewSet(GenericViewSet, AutoPrefetchMixin, PreloadIncludesMixin, RelatedMixin):
    """boundary view of user module"""
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    @action(detail=False, url_path="me")
    def get_userinfo(self, request: Request) -> Response:
        serializer = self.get_serializer(request.user)
        return Response(serializer.data)
