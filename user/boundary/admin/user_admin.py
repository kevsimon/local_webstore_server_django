"""
@author Mattis Rosenkranz
"""

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from user.entity.models import User


@admin.register(User)
class UserAdmin(UserAdmin):
    """defines the user module in Django Admin"""
    list_display = ["username", "email", "first_name", "last_name", "is_staff", "is_vendor"]
    list_filter = ["is_staff", "is_superuser", "is_active", "groups", "is_vendor"]
