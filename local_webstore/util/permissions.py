"""
@author Mattis Rosenkranz
"""

from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.views import APIView


class IsVendor(IsAuthenticated):
    """returns the permission of a vendor"""
    def has_permission(self, request: Request, view: APIView):
        is_authenticated = super().has_permission(request, view)
        return is_authenticated and (request.user.is_vendor or request.user.is_superuser)
