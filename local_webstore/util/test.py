import json
from abc import ABC
from typing import Optional, List

import requests
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.db.models import Model
from django.test import TestCase
from requests.auth import HTTPBasicAuth

from local_webstore import settings


class Relationship:
    def __init__(self, resource_name, pk):
        self.resource_name = resource_name
        self.pk = pk

    def format_to_data(self):
        return {
            "type": self.resource_name,
            "id": str(self.pk),
        }


class JsonApiTestCase(ABC, TestCase):
    ENDPOINT_URL = ""
    JSON_API_CONTENT_TYPE = "application/vnd.api+json"

    @classmethod
    def setUpTestData(cls):
        assert cls.ENDPOINT_URL
        cls.token = cls.get_oauth_test_token()

    @classmethod
    def pretty_print_dict(cls, data: dict):
        """
        Prints a dict using multiple lines and indents in the console
        """

        print(json.dumps(data, indent=4))

    @staticmethod
    def get_oauth_test_token() -> Optional[str]:
        """
        Gets a token with the prefix 'Bearer' from the oauth provider using the oidc and test settings
        :return: The token or None on failure
        """

        response = requests.post(
            settings.OIDC_OP_TOKEN_ENDPOINT,
            auth=HTTPBasicAuth(settings.OIDC_RP_CLIENT_ID, settings.OIDC_RP_CLIENT_SECRET),
            data={
                "username": settings.OIDC_TEST_EMAIL,
                "password": settings.OIDC_TEST_PASSWORD,
                "grant_type": "password"
            },
            headers={
                "Content-Type": "application/x-www-form-urlencoded"
            }
        )

        if response.status_code == 200:
            return f"Bearer {response.json()['access_token']}"
        return None

    @staticmethod
    def create_test_user():
        return get_user_model().objects.create_user(settings.OIDC_TEST_ID, settings.OIDC_TEST_EMAIL)

    @staticmethod
    def load_data(file_path):
        call_command('loaddata', file_path, verbosity=0)

    @classmethod
    def get_default_request_kwargs(cls) -> dict:
        return {
            "HTTP_AUTHORIZATION": cls.token,
            "content_type": cls.JSON_API_CONTENT_TYPE
        }

    def create(self, data: dict):
        return self.client.post(f"{self.ENDPOINT_URL}/", data, **self.get_default_request_kwargs())

    def list(self, query_parameters: dict = None):
        return self.client.get(f"{self.ENDPOINT_URL}/", query_parameters, **self.get_default_request_kwargs())

    def retrieve(self, instance: Model, query_parameters: dict = None):
        return self.client.get(f"{self.ENDPOINT_URL}/{instance.pk}/", query_parameters, **self.get_default_request_kwargs())

    def update(self, instance: Model, data: dict):
        return self.client.patch(f"{self.ENDPOINT_URL}/{instance.pk}/", data, **self.get_default_request_kwargs())

    def delete(self, instance: Model):
        return self.client.delete(f"{self.ENDPOINT_URL}/{instance.pk}/", **self.get_default_request_kwargs())

    @staticmethod
    def format_relationships(relationships: dict):
        data = {}
        for key in relationships:
            relationship = relationships[key]
            if isinstance(relationship, Relationship):
                data[key] = {"data": relationships[key].format_to_data()}
            if isinstance(relationship, list):
                data[key] = {
                    "data": [relationship_item.format_to_data() for relationship_item in relationship
                             if isinstance(relationship_item, Relationship)]
                }
        return data

    @classmethod
    def format_data(cls, resource_name: str, attributes: dict, relationships: dict, pk: str = None):
        data = {
            "data": {
                "type": resource_name,
                "attributes": attributes,
                "relationships": cls.format_relationships(relationships)
            }
        }
        if pk:
            data["data"]["id"] = pk
        return data

    @classmethod
    def assert_attributes_match(cls, attributes: dict, expected_attributes: dict, parent_key="attributes"):
        for key in expected_attributes:
            assert key in attributes, f"Expected key '{key}' to be in '{parent_key}'"
            value = attributes[key]
            expected_value = expected_attributes[key]
            assert type(value) == type(expected_value), f"Expected value of '{key}' in '{parent_key}' " \
                                                        f"to be of type '{type(expected_value)}'"
            if isinstance(value, dict):
                cls.assert_attributes_match(value, expected_value, parent_key=key)

    @classmethod
    def assert_has_relationships(cls, relationships: dict, expected_relationships: List[str]):
        for key in expected_relationships:
            assert key in relationships, f"Expected key {key} to be in relationships"
