"""local_webstore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView, RedirectView
from rest_framework.permissions import IsAdminUser
from rest_framework.schemas import get_schema_view
from rest_framework.authentication import SessionAuthentication

from local_webstore.schema.openapi import SchemaGenerator

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('user.urls')),
    path('api/v1/vendor/', include('store.urls')),
    path('api/v1/vendor/', include('product.urls')),
    path(
        'api/v1/schema/',
        get_schema_view(
            generator_class=SchemaGenerator,
            authentication_classes=[SessionAuthentication],
            permission_classes=[IsAdminUser]
        ),
        name='openapi-schema'
    ),
    path(
        'swagger/',
        TemplateView.as_view(
            template_name='swagger-ui.html',
            extra_context={'schema_url': 'openapi-schema'}
        ),
        name='swagger-ui'
    ),
    path('', RedirectView.as_view(url='swagger/'))
]
