OIDC_TEST_ID = "1d1ded6d-adc7-40bc-ab61-52ee85469e4c"
OIDC_TEST_EMAIL = "manni@hs-osnabrueck.de"
OIDC_TEST_PASSWORD = "shopping"

OIDC_TEST_EXPECTED_USER_DATA = {
    "username": OIDC_TEST_ID,
    "email": OIDC_TEST_EMAIL,
    "first_name": "Manni",
    "last_name": "Gold",
    "is_vendor": True,
    "is_staff": False,
    "is_superuser": False
}
