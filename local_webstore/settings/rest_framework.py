# django-rest-framework
# https://www.django-rest-framework.org/
# django-rest-framework-json-api
# https://django-rest-framework-json-api.readthedocs.io/

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.AllowAny'
    ],
    # JSON:API
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework_json_api.renderers.JSONRenderer',
    ],
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework_json_api.parsers.JSONParser',
    ),
    'EXCEPTION_HANDLER': 'rest_framework_json_api.exceptions.exception_handler',
    'PAGE_SIZE': 10,
    'SEARCH_PARAM': 'filter[search]',
    'DEFAULT_PAGINATION_CLASS': 'rest_framework_json_api.pagination.JsonApiPageNumberPagination',
    'DEFAULT_METADATA_CLASS': 'rest_framework_json_api.metadata.JSONAPIMetadata',
    # OIDC
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'mozilla_django_oidc.contrib.drf.OIDCAuthentication',
    ),
    # OpenAPI
    'DEFAULT_SCHEMA_CLASS': 'local_webstore.schema.openapi.AutoSchema',
}


# JSON:API

JSON_API_FORMAT_FIELD_NAMES = 'camelize'


# Cors headers
# https://github.com/adamchainz/django-cors-headers

CORS_ALLOW_ALL_ORIGINS = True
