# Secret key used for encryption and hashing
SECRET_KEY = 'django-insecure-y5u46-rzrose!h-#(e9q7lu_g@d*o!io*hz%9t_7c^!vd19k7y'

# Hoster validation
ALLOWED_HOSTS = ["*"]

# Databases
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'local_webstore',
        'USER': 'local_webstore',
        'PASSWORD': 'local_webstore',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}
