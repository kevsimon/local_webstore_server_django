from .general import *
from .language import *
from .password_validation import *
from .rest_framework import *
from .oidc import *
from .testing import *


# Debug or production mode
DEBUG = True

if DEBUG:
    from .modes.debug import *
else:
    try:
        from .modes.production import *
    except ImportError:
        assert False, "Production mode is enabled but no production.py settings file " \
                      "is defined in local_webstore.settings.modes"
