# OpenID Connect
# mozilla-django-oidc
# https://mozilla-django-oidc.readthedocs.io/

OIDC_DRF_AUTH_BACKEND = 'local_webstore.oidc.auth.OIDCAuthenticationBackend'
OIDC_REALM = "django"
OIDC_RP_CLIENT_ID = "django_api"
OIDC_RP_CLIENT_SECRET = "f86814a7-fca2-4cac-9141-c1f1459aae1a"

_OIDC_BASE_URL = f"http://localhost:8180/auth/realms/{OIDC_REALM}/protocol/openid-connect"
OIDC_OP_AUTHORIZATION_ENDPOINT = f"{_OIDC_BASE_URL}/auth"
OIDC_OP_TOKEN_ENDPOINT = f"{_OIDC_BASE_URL}/token"
OIDC_OP_USER_ENDPOINT = f"{_OIDC_BASE_URL}/userinfo"
