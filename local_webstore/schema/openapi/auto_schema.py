from rest_framework_json_api.schemas.openapi import AutoSchema as JSONAPIAutoSchema


class AutoSchema(JSONAPIAutoSchema):
    def get_operation(self, path, method):
        operation = super().get_operation(path, method)
        operation["security"] = [
            {
                "OAuth Token": []
            }
        ]
        return operation
