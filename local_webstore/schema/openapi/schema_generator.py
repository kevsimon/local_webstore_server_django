from rest_framework_json_api.schemas.openapi import SchemaGenerator as JSONAPISchemaGenerator


class SchemaGenerator(JSONAPISchemaGenerator):
    def get_schema(self, request=None, public=False):
        schema = super().get_schema(request, public)
        schema["info"] = {
            "title": "Local Webstore",
            "description": "Local shopping made easier. To authenticate use the local webstore oauth provider.",
            "version": "1.0.0",
        }
        schema["components"]["securitySchemes"] = {
            "OAuth Token": {
                "type": "apiKey",
                "in": "header",
                "name": "Authorization",
                "description": "Token - based authentication with required prefix 'Bearer'"
            }
        }
        return schema
