from mozilla_django_oidc.auth import OIDCAuthenticationBackend as BaseOIDCAuthenticationBackend
from user.entity.models import User


class OIDCAuthenticationBackend(BaseOIDCAuthenticationBackend):
    def filter_users_by_claims(self, claims):
        user_id = claims.get("sub")
        if not user_id:
            return self.UserModel.objects.none()
        return self.UserModel.objects.filter(username__iexact=user_id)

    def create_user(self, claims: dict) -> User:
        username = claims.get('sub')
        email = claims.get('email')
        first_name = claims.get('given_name', '')
        last_name = claims.get('family_name', '')

        roles = claims.get("roles", [])
        is_vendor = "vendor" in roles

        return self.UserModel.objects.create_user(username, email=email, first_name=first_name,
                                                  last_name=last_name, is_vendor=is_vendor)

    def update_user(self, user: User, claims: dict):
        user_changed = False

        first_name = claims.get("given_name", None)
        last_name = claims.get("family_name", None)
        roles = claims.get("roles", [])
        is_vendor = "vendor" in roles

        if first_name and first_name != user.first_name:
            user.first_name = first_name
            user_changed = True

        if last_name and last_name != user.last_name:
            user.last_name = last_name
            user_changed = True

        if is_vendor != user.is_vendor:
            user.is_vendor = is_vendor
            user_changed = True

        if user_changed:
            user.save()

        return user
