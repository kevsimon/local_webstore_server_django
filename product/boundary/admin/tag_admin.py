"""
@author Mattis Rosenkranz
"""

from django.contrib import admin
from django.contrib.admin import ModelAdmin

from product.entity.models import Tag


@admin.register(Tag)
class TagAdmin(ModelAdmin):
    list_display = ["name", "store"]
