"""
@author Mattis Rosenkranz
"""

from django.contrib import admin
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin, PolymorphicChildModelFilter

from product.entity.models_base.product import Product
from product.entity.models import Book, Pencil


@admin.register(Book)
class BookAdmin(PolymorphicChildModelAdmin):
    show_in_index = True
    list_display = ["name", "price", "author", "isbn", "visible", "release_date"]


@admin.register(Pencil)
class PencilAdmin(PolymorphicChildModelAdmin):
    show_in_index = True
    list_display = ["name", "price", "degree_of_hardness", "has_eraser", "visible"]


@admin.register(Product)
class ProductAdmin(PolymorphicParentModelAdmin):
    child_models = [Book, Pencil]
    list_display = ["name", "price", "visible", "get_type"]
    list_filter = [PolymorphicChildModelFilter, "visible"]

    @admin.display(description="type")
    def get_type(self, instance):
        return str(instance.polymorphic_ctype.model).capitalize()
