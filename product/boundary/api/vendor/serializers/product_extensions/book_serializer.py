"""
@author Mattis Rosenkranz
"""

from product.boundary.api.vendor.serializers_base import ProductSerializerBase
from product.entity.models.product_extensions.book import Book


class BookSerializer(ProductSerializerBase):
    class Meta(ProductSerializerBase.Meta):
        model = Book
        fields = "__all__"
