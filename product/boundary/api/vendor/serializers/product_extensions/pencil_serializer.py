"""
@author Mattis Rosenkranz
"""

from product.boundary.api.vendor.serializers_base import ProductSerializerBase
from product.entity.models import Pencil


class PencilSerializer(ProductSerializerBase):
    class Meta(ProductSerializerBase.Meta):
        model = Pencil
        fields = "__all__"
