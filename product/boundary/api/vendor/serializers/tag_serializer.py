"""
@author Mattis Rosenkranz
"""

from typing import List

from rest_framework_json_api import serializers

from product.boundary.api.vendor.fields import StoreResourceRelatedField
from product.boundary.api.vendor.validators.tag_validators import TagUniqueForStoreValidator
from product.entity.models import Tag
from store.entity.models import Store


class TagSerializer(serializers.ModelSerializer):
    """serializer for tags"""
    store = StoreResourceRelatedField(queryset=Store.objects.all())

    def is_updating(self) -> bool:
        view = self.context.get("view")
        return view and view.action in ["update", "partial_update"]

    def get_field_names(self, declared_fields, info) -> List[str]:
        # default behaviour
        field_names = super().get_field_names(declared_fields, info)

        # remove store if in update mode, original list can include duplicates
        if self.is_updating():
            field_names = [field_name for field_name in field_names if field_name != "store"]

        return field_names

    class Meta:
        model = Tag
        fields = "__all__"
        validators = [TagUniqueForStoreValidator()]

    included_serializers = {
        'store': 'store.boundary.api.vendor.serializers.StoreSerializer'
    }
