"""
@author Mattis Rosenkranz
"""

from django.db.models import QuerySet
from rest_framework_json_api.relations import ResourceRelatedField

from product.control.adapters import get_accessible_stores_for_user


class StoreResourceRelatedField(ResourceRelatedField):
    """store related fields"""
    def get_queryset(self) -> QuerySet:
        user = self.context["request"].user
        return get_accessible_stores_for_user(user)
