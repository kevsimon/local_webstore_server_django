"""
@author Mattis Rosenkranz
"""

from django.db.models import QuerySet
from rest_framework.exceptions import ValidationError
from rest_framework.filters import BaseFilterBackend
from rest_framework.request import Request
from rest_framework.views import APIView


class VisibilityFilter(BaseFilterBackend):
    """filter for visibility"""
    QUERY_PARAM = "filter[visible]"

    def filter_queryset(self, request: Request, queryset: QuerySet, view: APIView) -> QuerySet:
        visible = request.query_params.get(self.QUERY_PARAM)
        if visible:
            # check for wrong formatting
            if not visible.lower() in ["true", "false"]:
                raise ValidationError(f"Query param {self.QUERY_PARAM} must be 'true' or 'false'")
            # convert to bool and apply filter
            visible = visible.lower() == "true"
            queryset = queryset.filter(visible=visible)
        return queryset

    def get_schema_operation_parameters(self, view):
        return [
            {
                'name': self.QUERY_PARAM,
                'required': False,
                'in': 'query',
                'description': "Filter for public visibility. Valid values are 'true' and 'false'",
                'schema': {
                    'type': 'string',
                }
            },
        ]
