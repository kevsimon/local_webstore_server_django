"""
@author Mattis Rosenkranz
"""

from django.db.models import QuerySet
from rest_framework.exceptions import ValidationError
from rest_framework.filters import BaseFilterBackend
from rest_framework.request import Request
from rest_framework.views import APIView


class StoreFilter(BaseFilterBackend):
    """filter for stores"""
    QUERY_PARAM = "filter[store-id]"

    def filter_queryset(self, request: Request, queryset: QuerySet, view: APIView) -> QuerySet:
        store_id = request.query_params.get(self.QUERY_PARAM)
        if not store_id:
            raise ValidationError(f"Missing required query parameter '{self.QUERY_PARAM}'")
        return queryset.filter(store_id=store_id)

    def get_schema_operation_parameters(self, view):
        return [
            {
                'name': self.QUERY_PARAM,
                'required': True,
                'in': 'query',
                'description': "Id of the store to retrieve from",
                'schema': {
                    'type': 'integer',
                }
            },
        ]
