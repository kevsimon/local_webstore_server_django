"""
@author Mattis Rosenkranz
"""

from rest_framework_json_api import serializers

from product.boundary.api.vendor.serializers import BookSerializer, PencilSerializer
from product.entity.models_base import Product


class ProductSerializerPolymorphic(serializers.PolymorphicModelSerializer):
    """polymorphic product serializer"""
    polymorphic_serializers = [BookSerializer, PencilSerializer]

    class Meta:
        model = Product
        fields = "__all__"

    included_serializers = {
        'store': 'store.boundary.api.vendor.serializers.StoreSerializer',
        'tags': 'product.boundary.api.vendor.serializers.TagSerializer'
    }
