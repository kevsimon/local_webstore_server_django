"""
@author Mattis Rosenkranz
"""

from rest_framework.filters import SearchFilter
from rest_framework_json_api.filters import OrderingFilter
from rest_framework_json_api.views import ModelViewSet

from local_webstore.util.permissions import IsVendor
from product.boundary.api.vendor.filters import StoreFilter
from product.boundary.api.vendor.serializers import TagSerializer
from product.entity.models import Tag


class TagViewSet(ModelViewSet):
    """viewset for tags"""
    serializer_class = TagSerializer
    permission_classes = [IsVendor]
    search_fields = ["name"]
    ordering_fields = ["name"]

    @property
    def filter_backends(self):
        filters = [SearchFilter, OrderingFilter]
        if self.action == "list":
            filters += [StoreFilter]
        return filters

    def get_queryset(self, *args, **kwargs):
        return Tag.objects.accessible_to_user(self.request.user)
