"""
@author Mattis Rosenkranz
"""

from rest_framework.filters import SearchFilter
from rest_framework_json_api.filters import OrderingFilter
from rest_framework_json_api.views import ModelViewSet

from local_webstore.util.permissions import IsVendor
from product.boundary.api.vendor.filters import StoreFilter, VisibilityFilter
from product.boundary.api.shared.filters import TagFilter
from product.boundary.api.vendor.serializers_polymorphic import ProductSerializerPolymorphic
from product.entity.models_base import Product


class ProductViewSet(ModelViewSet):
    """viewset for products"""
    serializer_class = ProductSerializerPolymorphic
    permission_classes = [IsVendor]
    search_fields = ["name"]
    ordering_fields = ["name", "price", "created_at", "updated_at"]

    @property
    def filter_backends(self):
        filters = [SearchFilter, OrderingFilter]
        if self.action == "list":
            filters += [StoreFilter, VisibilityFilter, TagFilter]
        return filters

    def get_queryset(self, *args, **kwargs):
        return Product.objects.accessible_to_user(self.request.user)
