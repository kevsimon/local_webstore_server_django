"""
@author Mattis Rosenkranz
"""

from rest_framework.exceptions import ValidationError
from rest_framework.serializers import Serializer

from product.entity.models import Tag


class TagUniqueForStoreValidator:
    """tag validator"""
    requires_context = True
    _MESSAGE = "The fields name, store must make a unique set"

    def _validate_create(self, attrs: dict):
        if Tag.objects.filter(store=attrs["store"], name=attrs["name"]).exists():
            raise ValidationError(self._MESSAGE)

    def _validate_update(self, instance, attrs: dict):
        if "name" in attrs:
            if Tag.objects.exclude(id=instance.id).filter(store=instance.store, name=attrs["name"]).exists():
                raise ValidationError(self._MESSAGE)

    def __call__(self, attrs: dict, serializer: Serializer):
        instance = serializer.instance
        if not instance:
            self._validate_create(attrs)
        else:
            self._validate_update(instance, attrs)
        return attrs
