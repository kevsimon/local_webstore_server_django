"""
@author Mattis Rosenkranz
"""

from rest_framework.exceptions import ValidationError
from rest_framework.serializers import Serializer


class TagsBelongToStoreValidator:
    """product validator"""
    requires_context = True

    def __call__(self, attrs: dict, serializer: Serializer):
        # check if tags are updated
        tags = attrs.get("tags")
        if tags:
            # get new store
            store = attrs.get("store")
            # on update without new store use existing store
            if not store:
                store = serializer.instance.store
            # check if tags belong to store
            for tag in tags:
                if tag.store_id != store.id:
                    raise ValidationError({"tags": "Must be in store of product"})
        return attrs


class PriceValidator:
    def __call__(self, price: float):
        if price < 0:
            raise ValidationError({"price": "Must be a positive number"})
