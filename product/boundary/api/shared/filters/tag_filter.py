"""
@author Mattis Rosenkranz
"""

import operator
from functools import reduce

from django.db.models import QuerySet, Q
from rest_framework.filters import BaseFilterBackend
from rest_framework.request import Request
from rest_framework.views import APIView


class TagFilter(BaseFilterBackend):
    """filter for tags"""
    QUERY_PARAM = "filter[tag-ids]"

    def filter_queryset(self, request: Request, queryset: QuerySet, view: APIView) -> QuerySet:
        # get tag ids
        tag_ids = request.query_params.getlist(self.QUERY_PARAM)

        if tag_ids:
            # build filter by using conditions chained with 'and'
            conditions = [Q(tags__id=tag_id) for tag_id in tag_ids]
            query = reduce(operator.and_, conditions)
            # exclude all elements that do not have the given tags
            queryset = queryset.exclude(~query)

        return queryset

    def get_schema_operation_parameters(self, view):
        return [
            {
                'name': self.QUERY_PARAM,
                'required': False,
                'in': 'query',
                'description': "List of tag ids to filter for",
                'schema': {
                    'type': 'array',
                    'items': {
                        'type': 'integer'
                    },
                }
            },
        ]
