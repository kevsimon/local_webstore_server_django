from rest_framework import routers

from product.boundary.api.vendor.views import TagViewSet, ProductViewSet

router = routers.SimpleRouter()
router.register('tags', TagViewSet, 'Tag')
router.register('products', ProductViewSet, 'Product')
urlpatterns = router.urls
