from .tag import Tag
from .product_extensions.book import Book
from .product_extensions.pencil import Pencil
