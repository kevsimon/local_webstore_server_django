"""
@author Mattis Rosenkranz
"""

from django.db import models

from product.control.managers import TagManager


class Tag(models.Model):
    """tag entity"""
    name = models.TextField()
    store = models.ForeignKey("store.Store", on_delete=models.CASCADE)

    objects = TagManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(fields=['name', 'store'], name='tag_unique_for_store'),
        ]
