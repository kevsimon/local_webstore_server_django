"""
@author Mattis Rosenkranz
"""

from product.entity.models_base import Product
from django.db import models


class Book(Product):
    """book entity"""
    isbn = models.TextField()
    author = models.TextField()
    release_date = models.DateField()

    class JSONAPIMeta:
        resource_name = "Product_Book"
