"""
@author Mattis Rosenkranz
"""

from product.entity.models_base import Product
from django.db import models


class Pencil(Product):
    """pencil entity"""
    degree_of_hardness = models.TextField()
    has_eraser = models.BooleanField()

    class JSONAPIMeta:
        resource_name = "Product_Pencil"
