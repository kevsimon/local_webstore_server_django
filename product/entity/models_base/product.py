from polymorphic.models import PolymorphicModel
from django.db import models

from product.control.managers import ProductManager


class Product(PolymorphicModel):
    name = models.TextField()
    description = models.TextField()
    price = models.DecimalField(max_digits=8, decimal_places=2)
    visible = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    store = models.ForeignKey("store.Store", on_delete=models.CASCADE)
    tags = models.ManyToManyField("Tag", blank=True)

    objects = ProductManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
