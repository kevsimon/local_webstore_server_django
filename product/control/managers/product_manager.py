"""
@author Mattis Rosenkranz
"""

from polymorphic.managers import PolymorphicManager

from django.db.models import QuerySet

from product.control.adapters import get_accessible_stores_for_user
from user.entity.models import User


class ProductManager(PolymorphicManager):
    def accessible_to_user(self, user: User) -> QuerySet:
        return self.filter(store__in=get_accessible_stores_for_user(user))
