"""
@author Mattis Rosenkranz
"""

from user.entity.models import User
from store.control.anti_corruption_layer import get_accessible_stores_for_user as store_get_accessible_stores_for_user


def get_accessible_stores_for_user(user: User):
    """adapter for store"""
    return store_get_accessible_stores_for_user(user)
