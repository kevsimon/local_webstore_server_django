"""
@author Mattis Rosenkranz
"""

from local_webstore.util.test import JsonApiTestCase, Relationship
from product.entity.models import Book, Pencil, Tag
from product.entity.models_base import Product
from store.entity.models import Store


class ProductTest(JsonApiTestCase):
    """test class for the product boundary"""
    ENDPOINT_URL = "/api/v1/vendor/products"

    EXAMPLE_ATTRIBUTES_BOOK = {
        "name": "Zauberstäbe für Dummies",
        "description": "Alles was sie für den Einstieg in die Welt der Zauberstäbe wissen müssen.",
        "price": 20,
        "visible": True,
        "isbn": "12345",
        "author": "Albus Dumbledore",
        "release_date": "2012-04-23"
    }

    EXAMPLE_ATTRIBUTES_PENCIL = {
        "name": "Magischer Bleistift",
        "description": "Zeichnet alles was sie ihm sagen. Wenn auch auf Niveau eines Sechstklässlers. "
                       "Wird mürrisch wenn lange unbenutzt.",
        "price": 5,
        "visible": True,
        "degree_of_hardness": "H6",
        "has_eraser": True,
    }

    # ----- create -----

    def test_create_book(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("product/tests/data/tags/mannis.json")

        relationships = {
            "store": Relationship("Store", 4),
            "tags": [Relationship("Tag", 1), Relationship("Tag", 2)]
        }

        data = self.format_data("Product_Book", self.EXAMPLE_ATTRIBUTES_BOOK, relationships)
        response = self.create(data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Product.objects.exists())
        self.assertTrue(Book.objects.exists())

    def test_create_pencil(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("product/tests/data/tags/mannis.json")

        relationships = {
            "store": Relationship("Store", 4),
            "tags": [Relationship("Tag", 1), Relationship("Tag", 2)]
        }

        data = self.format_data("Product_Pencil", self.EXAMPLE_ATTRIBUTES_PENCIL, relationships)
        response = self.create(data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Product.objects.exists())
        self.assertTrue(Pencil.objects.exists())

    def test_create_with_store_not_owned_invalid(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("store/tests/data/ollivanders.json")

        relationships = {
            "store": Relationship("Store", 1),
            "tags": []
        }

        data = self.format_data("Product_Book", self.EXAMPLE_ATTRIBUTES_BOOK, relationships)
        response = self.create(data)
        self.assertEqual(response.status_code, 400)

    def test_create_with_tag_of_other_store_invalid(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("store/tests/data/ollivanders.json")
        self.load_data("product/tests/data/tags/ollivanders.json")
        Store.objects.filter(name="Ollivanders").update(owner_id=4)

        relationships = {
            "store": Relationship("Store", 4),
            "tags": [Relationship("Tag", 3)]
        }

        data = self.format_data("Product_Book", self.EXAMPLE_ATTRIBUTES_BOOK, relationships)
        response = self.create(data)
        self.assertEqual(response.status_code, 400)

    def test_create_with_negative_price_invalid(self):
        self.load_data("store/tests/data/mannis.json")

        relationships = {
            "store": Relationship("Store", 4),
            "tags": []
        }

        attributes = self.EXAMPLE_ATTRIBUTES_BOOK.copy()
        attributes["price"] = -3

        data = self.format_data("Product_Book", attributes, relationships)
        response = self.create(data)
        self.assertEqual(response.status_code, 400)

    # ----- list -----

    def test_list(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("product/tests/data/products/mannis.json")

        # check response
        response = self.list({
            "filter[store-id]": 4,
            "include": "store,tags",
        })
        self.assertEqual(response.status_code, 200)

        # check polymorphic types
        data = response.json()
        product_types = [product["type"] for product in data["data"]]
        self.assertTrue("Product_Book" in product_types)
        self.assertTrue("Product_Pencil" in product_types)

    def test_list_without_store_id_invalid(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("product/tests/data/products/mannis.json")

        # check response
        response = self.list()
        self.assertEqual(response.status_code, 400)

    def test_list_with_tag_filter(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("product/tests/data/products/mannis.json")
        self.load_data("product/tests/data/tags/mannis.json")
        book = Book.objects.get()
        pencil = Pencil.objects.get()

        tag_1 = Tag.objects.get(id=1)
        tag_2 = Tag.objects.get(id=2)

        book.tags.add(tag_1)
        pencil.tags.add(tag_1, tag_2)

        response = self.list({
            "filter[store-id]": 4,
            "filter[tag-ids]": [1, 2]
        })
        self.assertEqual(response.status_code, 200)
        results = response.json()["data"]
        self.assertEqual(len(results), 1)

    def test_list_with_visibility_filter(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("product/tests/data/products/mannis.json")

        # make the book invisible to public
        Book.objects.update(visible=False)

        # filter for visible products
        response = self.list({
            "filter[store-id]": 4,
            "filter[visible]": True
        })

        # assert successful and only the pencil returned
        self.assertEqual(response.status_code, 200)
        results = response.json()["data"]
        self.assertEqual(len(results), 1)

    # ----- retrieve -----

    def test_retrieve(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("product/tests/data/products/mannis.json")

        book = Book.objects.get()
        response = self.retrieve(book)
        self.assertEqual(response.status_code, 200)

    def test_retrieve_with_store_not_owned_invalid(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("store/tests/data/ollivanders.json")
        self.load_data("product/tests/data/products/ollivanders.json")

        book_not_owned = Book.objects.get()
        response = self.retrieve(book_not_owned)
        self.assertEqual(response.status_code, 404)

    # ----- update -----

    def test_update(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("product/tests/data/products/mannis.json")
        self.load_data("product/tests/data/tags/mannis.json")

        attributes = {
            "name": "Zaubertränke für Fortgeschrittene"
        }

        relationships = {
            "tags": [Relationship("Tag", 1)]
        }

        book = Book.objects.get()
        data = self.format_data("Product_Book", attributes, relationships, pk=book.id)

        response = self.update(book, data)
        self.assertEqual(response.status_code, 200)

        book.refresh_from_db()
        tag_added = Tag.objects.get(id=1)
        self.assertEqual(book.name, "Zaubertränke für Fortgeschrittene")
        self.assertTrue(tag_added in book.tags.all())

    def test_update_store_invalid(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("store/tests/data/ollivanders.json")
        self.load_data("product/tests/data/products/mannis.json")

        # Change owner to authenticated test user
        Store.objects.filter(name="Ollivanders").update(owner_id=4)

        relationships = {
            "store": Relationship("Store", 1)
        }

        book = Book.objects.get()
        data = self.format_data("Product_Book", {}, relationships, pk=book.id)

        # update
        self.update(book, data)

        # assert that store didn't change
        book.refresh_from_db()
        self.assertEqual(book.store.id, 4)

    # ----- delete -----

    def test_delete(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("product/tests/data/products/mannis.json")

        book = Book.objects.get()
        response = self.delete(book)
        self.assertEqual(response.status_code, 204)
