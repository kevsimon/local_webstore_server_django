"""
@author Mattis Rosenkranz
"""

from local_webstore.util.test import JsonApiTestCase, Relationship
from product.entity.models import Tag
from store.entity.models import Store


class TagTest(JsonApiTestCase):
    """test class for the tag boundary"""
    ENDPOINT_URL = "/api/v1/vendor/tags"

    def test_create(self):
        self.load_data("store/tests/data/mannis.json")

        attributes = {"name": "wine"}
        relationships = {"store": Relationship("Store", 4)}
        data = self.format_data("Tag", attributes, relationships)

        response = self.create(data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Tag.objects.exists())

    def test_create_duplicate_tag_invalid(self):
        self.load_data("store/tests/data/mannis.json")
        attributes = {"name": "wine"}
        relationships = {"store": Relationship("Store", 4)}
        data = self.format_data("Tag", attributes, relationships)
        response = self.create(data)
        self.assertEqual(response.status_code, 201)
        response = self.create(data)
        self.assertEqual(response.status_code, 400)

    def test_create_with_store_not_owned(self):
        self.load_data("store/tests/data/ollivanders.json")
        attributes = {"name": "wands"}
        relationships = {"store": Relationship("Store", 1)}
        data = self.format_data("Tag", attributes, relationships)
        response = self.create(data)
        self.assertEqual(response.status_code, 400)

    def test_list(self):
        self.load_data("store/tests/data/mannis.json")
        Tag.objects.create(name="wine", store_id=4)
        response = self.list({
            "filter[store-id]": 4,
            "include": "store",
        })
        self.assertEqual(response.status_code, 200)
        response_data = response.json()["data"]
        self.assertEqual(len(response_data), 1)

    def test_list_store_not_owned_empty(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("store/tests/data/ollivanders.json")
        Tag.objects.create(name="wine", store_id=4)
        Tag.objects.create(name="furniture", store_id=1)
        response = self.list({
            "filter[store-id]": 1,
            "include": "store",
        })
        self.assertEqual(response.status_code, 200)
        response_data = response.json()["data"]
        self.assertEqual(len(response_data), 0)

    def test_list_without_store_id_invalid(self):
        self.load_data("store/tests/data/mannis.json")
        Tag.objects.create(name="wine", store_id=4)
        response = self.list()
        self.assertEqual(response.status_code, 400)

    def test_retrieve(self):
        self.load_data("store/tests/data/mannis.json")
        tag = Tag.objects.create(name="wine", store_id=4)
        response = self.retrieve(tag, {"include": "store"})
        self.assertEqual(response.status_code, 200)
        data = response.json()["data"]
        attributes = data["attributes"]
        relationships = data.get("relationships", [])
        self.assert_attributes_match(attributes, {"name": ""})
        self.assert_has_relationships(relationships, ["store"])

    def test_retrieve_with_store_not_owned_invalid(self):
        self.load_data("store/tests/data/ollivanders.json")
        tag = Tag.objects.create(name="wands", store_id=1)
        response = self.retrieve(tag)
        self.assertEqual(response.status_code, 404)

    def test_update(self):
        self.load_data("store/tests/data/mannis.json")
        attributes = {"name": "furniture"}
        tag = Tag.objects.create(name="wine", store_id=4)
        data = self.format_data("Tag", attributes, {}, tag.id)
        response = self.update(tag, data)
        self.assertEqual(response.status_code, 200)
        tag.refresh_from_db()
        self.assertEqual(tag.name, "furniture")

    def test_update_store_invalid(self):
        self.load_data("store/tests/data/mannis.json")
        self.load_data("store/tests/data/ollivanders.json")
        Store.objects.filter(id=1).update(owner_id=4)
        tag = Tag.objects.create(name="wine", store_id=4)

        relationships = {"store": Relationship("Store", 1)}
        data = self.format_data("Tag", {}, relationships, tag.id)

        self.update(tag, data)
        tag.refresh_from_db()
        self.assertEqual(tag.store.id, 4)

    def test_update_duplicate_name_invalid(self):
        self.load_data("store/tests/data/mannis.json")
        tag = Tag.objects.create(name="wine", store_id=4)
        Tag.objects.create(name="furniture", store_id=4)
        attributes = {"name": "furniture"}
        data = self.format_data("Tag", attributes, {}, tag.id)
        response = self.update(tag, data)
        self.assertEqual(response.status_code, 400)

    def test_delete(self):
        self.load_data("store/tests/data/mannis.json")
        tag = Tag.objects.create(name="wine", store_id=4)
        response = self.delete(tag)
        self.assertEqual(response.status_code, 204)
