from rest_framework import routers

from store.boundary.api.vendor.views import StoreViewSet

router = routers.SimpleRouter()
router.register('stores', StoreViewSet, 'Store')
urlpatterns = router.urls
