"""
@author Mattis Rosenkranz
"""

from django.db import models

from local_webstore import settings
from store.control.managers import StoreManager


class Store(models.Model):
    """store entity"""
    name = models.TextField()
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    description = models.TextField()
    phone_number = models.TextField()
    address = models.OneToOneField("Address", on_delete=models.CASCADE)

    objects = StoreManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
