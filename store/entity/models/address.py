"""
@author Mattis Rosenkranz
"""

from django.db import models

from store.control.managers import AddressManager


class Address(models.Model):
    """address entity"""
    street = models.TextField()
    street_number = models.TextField()
    postcode = models.TextField()
    city = models.TextField()

    objects = AddressManager()

    def __str__(self):
        return f"{self.street} {self.street_number}, {self.postcode} {self.city}"

    class Meta:
        verbose_name_plural = "Addresses"
