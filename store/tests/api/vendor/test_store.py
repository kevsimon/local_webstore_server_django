"""
@author Mattis Rosenkranz
"""

from local_webstore.util.test import JsonApiTestCase
from store.entity.models import Store


class StoreTest(JsonApiTestCase):
    """test class for stores"""
    ENDPOINT_URL = "/api/v1/vendor/stores"

    def test_create(self):
        attributes = {
            "name": "Ollivanders",
            "description": "Beste Zauberstäbe aus eigener Herstellung seit 382 v. Chr.",
            "phoneNumber": "12345",
            "address": {
                "street": "Winkelgasse",
                "street_number": "32",
                "postcode": "49625",
                "city": "London",
            }
        }
        data = self.format_data("Store", attributes, relationships={})
        response = self.create(data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Store.objects.exists())

    def test_create_with_empty_address_invalid(self):
        attributes = {
            "name": "Ollivanders",
            "description": "Beste Zauberstäbe aus eigener Herstellung seit 382 v. Chr.",
            "phone_number": "12345",
            "address": {}
        }
        data = self.format_data("Store", attributes, relationships={})
        response = self.create(data)
        self.assertEqual(response.status_code, 400)

    def test_list(self):
        self.load_data("store/tests/data/mannis.json")
        response = self.list()
        self.assertEqual(response.status_code, 200)
        response_data = response.json()["data"]
        self.assertEqual(len(response_data), 1)

    def test_retrieve(self):
        self.load_data("store/tests/data/mannis.json")
        store = Store.objects.get()
        response = self.retrieve(store)
        self.assertEqual(response.status_code, 200)
        data = response.json()["data"]
        attributes = data["attributes"]
        self.assert_attributes_match(attributes, {
            "name": "",
            "description": "",
            "phoneNumber": "",
            "address": {
                "street": "",
                "street_number": "",
                "postcode": "",
                "city": ""
            }
        })
        self.assertFalse("relationships" in data)

    def test_retrieve_not_owned(self):
        self.load_data("store/tests/data/ollivanders.json")
        store = Store.objects.get()
        response = self.retrieve(store)
        self.assertEqual(response.status_code, 404)

    def test_update(self):
        self.load_data("store/tests/data/mannis.json")
        store = Store.objects.get()
        attributes = {
            "name": "McDonalds",
            "address": {
                "street_number": "32 - 36"
            }
        }
        data = self.format_data("Store", attributes, {}, store.id)
        response = self.update(store, data)
        self.assertEqual(response.status_code, 200)

        store.refresh_from_db()
        self.assertEqual(store.name, "McDonalds")
        self.assertEqual(store.address.street_number, "32 - 36")

    def test_update_without_address(self):
        self.load_data("store/tests/data/mannis.json")
        store = Store.objects.get()
        attributes = {
            "name": "McDonalds",
        }
        data = self.format_data("Store", attributes, {}, store.id)
        response = self.update(store, data)
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        self.load_data("store/tests/data/mannis.json")
        store = Store.objects.get()
        response = self.delete(store)
        self.assertEqual(response.status_code, 204)
