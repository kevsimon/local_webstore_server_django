"""
@author Mattis Rosenkranz
"""

from store.entity.models import Store
from user.entity.models import User


def get_accessible_stores_for_user(user: User):
    """interface for store"""
    return Store.objects.accessible_to_user(user)
