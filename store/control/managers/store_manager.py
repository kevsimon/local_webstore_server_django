"""
@author Mattis Rosenkranz
"""

from django.db import models
from django.db.models import QuerySet

from user.entity.models import User


class StoreManager(models.Manager):
    def accessible_to_user(self, user: User) -> QuerySet:
        return self.filter(owner=user)
