"""
@author Mattis Rosenkranz
"""

from django.contrib import admin
from django.contrib.admin import ModelAdmin

from store.entity.models import Store


@admin.register(Store)
class StoreAdmin(ModelAdmin):
    """store module in Django Admin"""
    list_display = ["name", "owner", "description", "phone_number", "address"]

    # @admin.display(description='owner')
    # def get_owner(self, instance) -> str:
    #     return instance.owner.get_full_name()
