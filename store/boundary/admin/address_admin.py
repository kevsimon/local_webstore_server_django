"""
@author Mattis Rosenkranz
"""

from django.contrib import admin
from django.contrib.admin import ModelAdmin

from store.entity.models import Address


@admin.register(Address)
class AddressAdmin(ModelAdmin):
    """address module in Django Admin"""
    list_display = ["street", "street_number", "postcode", "city"]
