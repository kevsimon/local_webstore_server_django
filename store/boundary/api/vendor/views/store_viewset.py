"""
@author Mattis Rosenkranz
"""

from rest_framework_json_api.views import ModelViewSet

from local_webstore.util.permissions import IsVendor
from store.boundary.api.vendor.serializers import StoreSerializer
from store.entity.models import Store


class StoreViewSet(ModelViewSet):
    """boundary layer view set for stores"""
    serializer_class = StoreSerializer
    permission_classes = [IsVendor]

    def get_queryset(self, *args, **kwargs):
        return Store.objects.accessible_to_user(self.request.user).select_related("address")
