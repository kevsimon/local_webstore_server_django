"""
@author Mattis Rosenkranz
"""

from rest_framework_json_api import serializers

from store.entity.models import Store, Address


class AddressSerializer(serializers.ModelSerializer):
    """address serializer"""
    class Meta:
        model = Address
        fields = "__all__"


class StoreSerializer(serializers.ModelSerializer):
    """store serializer"""
    address = AddressSerializer()

    def validate(self, attrs: dict):
        address_data = attrs.get("address", {})
        AddressSerializer(data=address_data, partial=self.partial).is_valid(raise_exception=True)
        return attrs

    def create(self, validated_data: dict):
        owner = self.context["request"].user
        address_data = validated_data.pop("address")
        address = Address.objects.create(**address_data)
        return Store.objects.create(**validated_data, owner=owner, address=address)

    def update(self, instance: Store, validated_data: dict) -> Store:
        if "address" in validated_data:
            address_data = validated_data.pop("address")
            super().update(instance.address, address_data)
        return super().update(instance, validated_data)

    class Meta:
        model = Store
        fields = ["name", "description", "phone_number", "address"]
